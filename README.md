<<<<<<< HEAD
# limitless

> A frontend project to swipe on the retail products

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

To build the product for production first build the project
```
npm run build
```

copy the build output to the backend project where springboot will serve the html and javascipt.
```
cp -R dist/* ~/limitless-backend/public/
```
