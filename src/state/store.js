import Vuex from 'vuex';
import Vue from 'vue';
import axios from 'axios';
import * as a from './action-types';
import * as m from './mutation-types';

Vue.use(Vuex);
// var apiPrefix = process.env.API_PREFIX;
var apiPrefix = '';

const state = {
  currentRecommendation: {},
  currentChoices: {}
};

const mutations = {
  [m.SET_CHOICES] (state, choices) {
    state.currentChoices = choices;
  },
  [m.SET_RECOMMENDATION] (state, recommendation) {
    state.currentRecommendation = recommendation;
  },
  [m.RESET_RECOMMENDATION] (state, recommendation) {
    state.currentRecommendation = {};
  },
  [m.RESET_CHOICES] (state, recommendation) {
    state.currentChoices = {};
  }
};

const actions = {
  [a.LOAD_RECOMMENDATION] ({ commit }) {
    axios.get(apiPrefix + '/recommendation/').then((response) => {
      var recommendation = response.data;
      commit(m.SET_RECOMMENDATION, recommendation);
    }).catch((error) => {
      console.log('Error getting recommendation:' + error);
    });
  },
  [a.STORE_CHOICE] ({ commit }, choice) {
    axios.post(apiPrefix + '/choices/', choice).then((response) => {
    }).catch((error) => {
      console.log('Error putting choices:' + error);
    });
  },
  [a.LOAD_CHOICES] ({ commit }) {
    axios.get(apiPrefix + '/retailtree/').then((response) => {
      var choices = response.data;
      commit(m.SET_CHOICES, choices);
    }).catch((error) => {
      console.log('Error getting choices:' + error);
    });
  }
};

const getters = {
  choices: (state) => {
    return state.currentChoices;
  },
  recommendation: (state) => {
    return state.currentRecommendation;
  }
};

const store = new Vuex.Store({
  state,
  actions,
  mutations,
  getters
});

export default store;
export { store, mutations };
