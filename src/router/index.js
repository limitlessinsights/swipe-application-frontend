import Vue from 'vue';
import Router from 'vue-router';
import Swiping from '@/components/Swiping';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Swiping',
      component: Swiping
    }
  ]
});
